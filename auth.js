const jwt = require("jsonwebtoken");
const secret = "capstone2";

//Token Created
module.exports.createAccessToken = (result) => {
	const data = {
		id : result._id,
		username : result.username,
		isAdmin : result.isAdmin
	}
	return jwt.sign(data, secret, {});
}

//Token Verification
module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;

	if (token !== undefined){
	token = token.slice(7, token.length);
		console.log(token);
		return jwt.verify(token, secret, (error,data)=>{
			if (error){
				return response.send("Invalid Token");
			}
			else {
				next();
			}
		})
	}
	else {
		return response.send("Authentication failed! No token provided.")
	}

	
}


//Token Decryption
module.exports.decode = (token) => {
	if (token === undefined){
		return null
	}
	else{
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data)=>
		{
			if (error){
				return null;
			}else{
				
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
}