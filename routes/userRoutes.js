const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userControllers");

//Route for user registration
router.post("/signup", userController.signUp);

//Route for user login
router.post("/login", userController.logIn);

router.get("/profile", userController.profileDetails);

//Route to update Role
router.patch("/updateRole/:userId", auth.verify, userController.updateRole);

module.exports = router;
