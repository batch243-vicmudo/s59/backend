const express = require("express");
const router = express.Router();
const auth = require("../auth")
const orderControllers = require("../controllers/orderControllers")
//routes for user can add to cart
router.post("/addToCart", auth.verify, orderControllers.addToCart)

router.patch("/addToCartNext/:orderId", auth.verify, orderControllers.addToCartNext)

router.patch("/checkout/:orderId", auth.verify, orderControllers.checkout)
module.exports = router;