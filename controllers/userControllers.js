const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//User Registration
module.exports.signUp = (request, response) => {
  let newUser = new User({
    fullName: request.body.fullName,
    email: request.body.email,
    username: request.body.username,
    password: bcrypt.hashSync(request.body.password, 10),
  });

  return User.find({ username: request.body.username }).then((result) => {
    let usernameExists;
    if (result.length > 0) {
      return response.send({ usernameExists: true });
    } else {
      return newUser
        .save()
        .then((user) => {
          console.log(newUser);
          response.send({ usernameExists: false });
        })
        .catch((error) => {
          console.log(error);
          response.send(`Signup failed. Please try again.`);
        });
    }
  });
};

//User Authentication/Login
module.exports.logIn = (request, response) => {
  return User.findOne({ username: request.body.username }).then((result) => {
    if (result === null) {
      response.send(`Login failed. Please signup first.`);
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        request.body.password,
        result.password
      );

      if (isPasswordCorrect) {
        let token = auth.createAccessToken(result);
        console.log(token);
        return response.send({ accessToken: token });
      } else {
        return response.send(`Incorrect password, please try again.`);
      }
    }
  });
};

module.exports.profileDetails = (request, response) => {
  // user will be object that contains the id and email of the user that is currently logged in.
  const userData = auth.decode(request.headers.authorization);
  console.log(userData);

  return User.findById(userData.id)
    .then((result) => {
      result.password = "Confindential";
      return response.send(result);
    })
    .catch((err) => {
      return response.send(err);
    });
};

//Set User as Admin
module.exports.updateRole = (request, response) => {
  let token = request.headers.authorization;
  let userData = auth.decode(token);

  let idToBeUpdated = request.params.userId;

  if (userData.isAdmin) {
    return User.findById(idToBeUpdated)
      .then((result) => {
        let update = { isAdmin: !result.isAdmin };
        return User.findByIdAndUpdate(idToBeUpdated, update, { new: true })
          .then((document) => {
            document.password = "********";
            return response.send(document);
          })
          .catch((err) => response.send(err));
      })
      .catch((err) => response.send(err));
  } else {
    return response.send("You don't have access on this page!");
  }
};
