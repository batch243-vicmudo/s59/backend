const Product = require("../models/products");
const auth = require("../auth");

//Create new product
module.exports.addProduct = (request, response) => {
  let token = request.headers.authorization;
  const userData = auth.decode(token);
  let productExists;

  let newProduct = new Product({
    name: request.body.name,
    brand: request.body.brand,
    description: request.body.description,
    price: request.body.price,
    productType: request.body.productType,
    stock: request.body.stock,
  });

  if (userData.isAdmin) {
    newProduct
      .save()
      .then((result) => {
        console.log(result);
        response.send({ productExists: true });
      })
      .catch((error) => {
        console.log(error);
        response.send(`Failed to save your product details. Please try again.`);
      });
  } else {
    response.send("Your account is not an admin! Please contact the admin.");
  }
};

//Retrieve all active products
module.exports.activeProduct = (request, response) => {
  return Product.find({ isActive: true })
    .then((result) => {
      response.send(result);
    })
    .catch((error) => {
      response.send(`Failed to view all active Products.`);
    });
};

//Retrieve all Products
module.exports.allProduct = (request, response) => {
  let token = request.headers.authorization;
  const userData = auth.decode(token);

  if (userData.isAdmin) {
    return Product.find({})
      .then((result) => {
        response.send(result);
      })
      .catch((error) => {
        response.send(`Failed to view all listed Products.`);
      });
  } else {
    return response.send(
      "Your account is not an admin! Please contact the admin."
    );
  }
};

//Updating a product
module.exports.updateProduct = (request, response) => {
  const token = request.headers.authorization;
  const userData = auth.decode(token);
  let productExists;

  let updatedProduct = {
    name: request.body.name,
    brand: request.body.brand,
    description: request.body.description,
    price: request.body.price,
    productType: request.body.productType,
    stock: request.body.stock,
  };

  const productsId = request.params.productId;
  if (userData.isAdmin) {
    return Product.findByIdAndUpdate(productsId, updatedProduct, { new: true })
      .then((result) => {
        response.send({ productExists: true });
      })
      .catch((error) => {
        response.send(error);
      });
  } else {
    return response.send(
      "Your account is not an admin! Please contact the admin."
    );
  }
};

//Archive Products
module.exports.archiveProduct = (request, response) => {
  let token = request.headers.authorization;
  const userData = auth.decode(token);
  let archieveDone;

  let productArchive = {
    isActive: false,
  };

  const productId = request.params.productId;
  if (userData.isAdmin) {
    return Product.findByIdAndUpdate(productId, productArchive, { new: true })
      .then((result) => {
        response.send({ archieveDone: true });
      })
      .catch((error) => {
        response.send(`Failed to archive the product`);
      });
  } else {
    return response.send(
      "Your account is not an admin! Please contact the admin."
    );
  }
};

//Unarchive Products
module.exports.unarchiveProduct = (request, response) => {
  let token = request.headers.authorization;
  const userData = auth.decode(token);
  let unarchieveDone;
  let productUnarchive = {
    isActive: true,
  };

  const productId = request.params.productId;
  if (userData.isAdmin) {
    return Product.findByIdAndUpdate(productId, productUnarchive, { new: true })
      .then((result) => {
        response.send({ unarchieveDone: true });
      })
      .catch((error) => {
        response.send(`Failed to unarchive the product`);
      });
  } else {
    return response.send(
      "Your account is not an admin! Please contact the admin."
    );
  }
};

//retrieve single product
module.exports.retrieveSpecificProduct = (request, response) => {
  const productId = request.params.productId;
  return Product.findById(productId)
    .then((result) => {
      response.send(result);
    })
    .catch((error) => {
      response.send(error);
    });
};
